var scheduler = function() {
	try {
		if (scheduler.created) throw 'Already created';
	} catch (e) {
		console.error(e);
	};

	internals = {
		functions: {},
		level: 0,
		node: 0,
		bTree: {
			btSMF: function(level, node) {
				return node + (1 << level) - 1;
			},
			idxToLevelNode: function(idx) {
				(function iterateToLevelAndNode(level, node) {
					if (idx === (node + (1 << level) - 1)) {
						console.log(level, node, idx);
					} else if (node < ((1 << level) - 1)) {
						return iterateToLevelAndNode(level, node + 1);
					} else if (node === ((1 << level) - 1)) {
						return iterateToLevelAndNode(level + 1, 0);
					} else {
						console.log('that\'s weird');
					}
				}(0, 0));
			},
			nodes: [],
			setNode: function(value, level, node) {
				this.nodes[this.btSMF(level, node)] = value;
			},
			getNode: function(level, node) {
				return this.nodes[this.btSMF(level, node)];
			},
			removeNode: function(func) {
				var internals = this;
				this.nodes.find(function(val, idx) {
					(val === func) ? internals.nodes.splice(idx, 1): false;
				});
			},
			increment: function() {
				/*
				at limit if node === level ^ 2
				level 0 -> node     0
				level 1 -> node   0, 1
				level 2 -> node 0, 1, 2, 3
			     	level 3 -> node 0, 1, 2, 3, 4, 5, 6, 7
				level 4 -> node 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15
				---
				ALTERNATIVE REPRESENTATION

				Storage location    0   1   2   3   4   5   6
				level               0 | 1     | 2
				node                0 | 0   1 | 0   1   2   3
				---
				Sl	7  8  9  10  11  12  13  14   15  16  17  18  19  20  21  22  23  24  25  26  27  28  29  30
				l	3                           | 4
				n	0  1  2  3   4   5   6   7  | 0   1   2   3   4   5   6   7   8   9   10  11  12  13  14  15
				*/
				var newLevel = function() {
					internals.level = internals.level + 1;
					internals.node = 0;
				};
				(internals.node === 1 << internals.level) ? newLevel(): internals.node = internals.node + 1;
			}
		},
		targetFunction: function(func, NS) {
			return (typeof NS == 'undefined' ? '_' : NS) + '_' + (typeof func == "function" ? func.name : func);
		},
		register: function(func, NS) {
			var key = this.targetFunction(func, NS);
			this.functions[key] = func;
			this.bTree.setNode(key, this.level, this.node);
			this.bTree.increment();
		},
		runAll: function() {
			var internals = this;
			this.bTree.nodes.forEach(function(idx) {
				internals.functions[idx]();
			});
		},
		runTargetFunc: function(func, NS) {
			this.functions[targetFunction(func, NS)]();
		},
		runTargetLN: function(level, node) {
			this.functions[this.bTree.getNode(level, node)]()
		},
		delete: function(func, NS) {
			delete this.functions[this.targetFunction(func, NS)];
			this.bTree.removeNode(this.targetFunction(func, NS));
		},
		exit: function(msg) {
			try {
				throw msg || 'Exiting - msg was undefined';
			} catch (e) {
				console.error(e);
			}
		},
		registerBefore: function(func, NS, before) {
			Array.prototype.isPrototypeOf(before) ? true : this.exit('bad arg :: registerBefore called :: third arg not an Array');
			var
				placesInTree = [],
				internals = this;
			//before = ['ab','de']
			//nodes = ['ab']
			//nodes = ['ac']
			//nodes = ['ab','de','fg',hi']
			//go over the length of the "before" list/array.
			//Record what idx each string in that list is at.
			//Insert the new func after the highest idx using idxToLevelNode(idx) function.
			before.find(function(val, idx) {
				typeof internals.functions[val] === 'undefined' ? placesInTree.push(idx) : false;
			});
			//placesInTree.max
			this.functions[key] = func;
			this.bTree.setNode(key, this.level, this.node);
		},
		registerAfter: function(func, NS, after) {
			Array.prototype.isPrototypeOf(before) ? true : this.exit('bad arg :: registerBefore called :: third arg not an Array');
		}
	};

	scheduler.created = true;
	return Object.create(internals);
};
