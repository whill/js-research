(function() {
	try {
		requirejs(['https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.0/TweenMax.min.js'], function() {});
	} catch (e) {
		console.error('TweenMax failed to load', e);
	}


	sides = 4,
		imgEles = $('.thumbImage'),
		paths = [],
		imgs = [],
		df = document.createDocumentFragment(),
		imgFront = document.createElement('img'),
		imgBack = document.createElement('img'),
		perspective = document.createElement('div').setAttribute('class', 'cubePerspective'),
		cube = document.createElement('div').setAttribute('class', 'cube');

	//$('.imageDisplayWrapper').append('<div class="cubePerspective"><div class="cube"></div><div class="cube"></div><div class="cube"></div><div class="cube"></div></div>');

	$(imgEles).each(function(idx, img) {
		paths.push($(img).attr('src'))
	});

	paths.forEach(function(path, idx) {
		var
			scene7Path = '//sugartown.scene7.com/is/image/sugartown/',
			imgNameRegex = /sugartown\/(\d+_\w+)\?/i,
			imgNameFront = imgNameRegex.exec(path)[1],
			imgNameBack = imgNameFront + '_a1',
			RIaaSRegex = /wid=(.*)\&hei=(.*)\&\$JPGHigh\$/i,
			matches = RIaaSRegex.exec(path),
			widthParam = parseInt(matches[1]),
			heightParam = parseInt(matches[2]),
			srcSet2X = '?wid=' + widthParam * 2 + '&hei=' + heightParam * 2 + ' 2x, ',
			srcSet3X = '?wid=' + widthParam * 3 + '&hei=' + heightParam * 3 + ' 3x',
			twoXFront = imgNameFront + srcSet2X,
			threeXFront = imgNameFront + srcSet3X,
			twoXBack = imgNameBack + srcSet2X,
			threeXBack = imgNameBack + srcSet3X;

		imgFront.setAttribute('src', scene7Path + imgNameFront + '?wid=' + widthParam + '&hei=' + heightParam);
		imgBack.setAttribute('src', scene7Path + imgNameBack + '?wid=' + widthParam + '&hei=' + heightParam);
		//sugartown.scene7.com/is/image/sugartown/17490_tropicalpink?wid=372&hei=596 2x, //sugartown.scene7.com/is/image/sugartown/17490_tropicalpink?wid=558&hei=894 3x, //sugartown.scene7.com/is/image/sugartown/17490_tropicalpink?wid=558&hei=894 3x
		imgFront.setAttribute('srcset', scene7Path + twoXFront + scene7Path + threeXFront);
		imgBack.setAttribute('srcset', scene7Path + twoXBack + scene7Path + threeXBack);
		imgs.push([imgFront.cloneNode(true), imgBack.cloneNode(true)]);
		//
		//Need to add more cube sides. Just 2 right now for fake test.
		//
	});

	imgs.forEach(function(img, idx) {
		cube.appendChild(img[0]).appendChild(img[1]);
		df.appendChild(cube.cloneNode(true));
	});

	//	$('.imageDisplayWrapper').append('<div class="cubePerspective"><div class="cube"></div><div class="cube"></div><div class="cube"></div><div class="cube"></div></div>');
	//	$('.imageDisplayWrapper').find('.imageDisplay, .hoverImage').css('display', 'none');
	CWrapper = $('.cubePerspective').eq(0);

	TweenMax.set(CWrapper, {
		rotationX: 0,
		rotationY: 0,
		transformStyle: 'preserve-3d'
	});

	TweenMax.to(CWrapper, 4, {
		rotationY: 90
	});
}());
